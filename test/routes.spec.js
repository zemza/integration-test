import express from "express"
import { after, before, describe, it } from "mocha";
import {expect} from "chai";
import routes from "../src/routes.js"
import { rgb_to_hex } from "../src/converter.js";

const app = express();
/** @type {import("node:http").Server} */
let server;
const BASE_URL = "https://niisku.lab.fi/~kardev/projects/color-picker";
const BASE_URL2 = "https://niisku.lab.fi/~kardev/projects/color-picker/rgb_to_hex?red=255&green=136&blue=0";
describe("REST API routes", () => {
    before(() => {
        app.use("/api/v1", routes);
        server = app.listen(3000, () => {});
    });
    it("can get response", async () => {
        const response = await fetch(`${BASE_URL}`);
        //const response = fetch(`${BASE_URL}`);
        expect(response.ok).to.be.true;
    });
    it("should convert RGB to HEX correctly", async () => {
        const response = await fetch(`${BASE_URL}/rgb_to_hex?red=255&green=136&blue=0`);
        const text = await response.text();
        //expect(text).to.equal("#ff8800");
    });
    after((done) => {
        server.close(() => done());
    });
});

export default routes;