import express from 'express';
import routes from './routes.js';

const app = express();
const port = 3000;
const host = "localhost";
//Endpoint - GET http://localhost:3000/
app.get('/', (req, res) => {
    res.status(200).send("Hello world!");
});

app.listen(port, host, () => {
    console.log(`http://${host}:${port}`);
});

app.use(`/api/v1`, routes);

/**
 * Current color.
 * @type {[number, number, number]} R, G, B
 */
const currentColor = [0, 255, 0];
let colorSetting = false;

/**
 * Slider DOM-elements
 * @typedef RGB_DOM
 * @property {HTMLInputElement} r
 * @property {HTMLInputElement} g
 * @property {HTMLInputElement} b
 */

/**
 * Gets slider DOM-elements
 * @returns {RGB_DOM}
 */
document.addEventListener("DOMContentLoaded", () => {
    const sliderDOMs = {
        r: document.getElementById("slider-r"),
        g: document.getElementById("slider-g"),
        b: document.getElementById("slider-b")
    }
    return sliderDOMs;
});
const getSliderDOMs = () => {
    /**
     * @type {RGB_DOM}
     */
    const sliderDOMs = {
        r: document.getElementById("slider-r"),
        g: document.getElementById("slider-g"),
        b: document.getElementById("slider-b")
    }
    return sliderDOMs;
}

const getValueDOMs = () => {
    /**
     * @type {RGB_DOM}
     */
    const valueDOMs = {
        r: document.getElementById("value-r"),
        g: document.getElementById("value-g"),
        b: document.getElementById("value-b"),
    }
    return valueDOMs;
}

const initColor = () => {
    colorSetting = true;
    updateUI();
    colorSetting = false;
};

/**
 * 
 * @param {string} color "r", "g" or "b"
 * @param {number} value 0-255
 */
const valueChange = (color, value) => {
    if (colorSetting == false) {
        colorSetting = true;
        if (color == "r") currentColor[0] = parseInt(value, 10);
        else if (color == "g") currentColor[1] = parseInt(value, 10);
        else if (color == "b") currentColor[2] = parseInt(value, 10);
        updateUI();
        colorSetting = false;
    }
};

/**
 * direct change RGB
 * @param {string} rgb_string 
 */
const rgbChange = (rgb_string) => {
    if (colorSetting == false) {
        colorSetting = true;
        try {
            const rgb = rgb_string.split('(')[1].replace(')','').replace(' ', '').split(',');
            const r = parseInt(rgb[0], 10);
            const g = parseInt(rgb[1], 10);
            const b = parseInt(rgb[2], 10);
            if (isNaN(r) | isNaN(g) | isNaN(b) ) {
                throw new Error('faulty input');
            } else if ((0 <= r <= 255) && (0 <= g <= 255) && (0 <= b <= 255)) {
                currentColor[0] = r;
                currentColor[1] = g;
                currentColor[2] = b;
            } else {
                throw new Error('not in range');
            }
            updateUI();
        } catch (err) {
            console.error(err);
            updateUI();
        }
        colorSetting = false;
    }
}

/**
 * Direct change HEX
 * @param {string} hex_string
 */
const hexChange = (hex_string) => {
    if (colorSetting == false) {
        colorSetting = true;
        try {
            const rgb = hex_to_rgb(hex_string);
            console.log(rgb);
            if (rgb.findIndex((e) => e == -1) != -1) throw new Error('failed conversion');
            currentColor[0] = rgb[0];
            currentColor[1] = rgb[1];
            currentColor[2] = rgb[2];
            console.log(currentColor);
        } catch (err) {
            console.error(err);
        }
        updateUI();
        colorSetting = false;
    }
}

const updateUI = () => {
    const sliderDOMs = getSliderDOMs();
    const valueDOMs = getValueDOMs();
    const color_area = document.getElementById("color-display-area");
    color_area.style = `background-color: rgb(${currentColor[0]}, ${currentColor[1]}, ${currentColor[2]});`
    sliderDOMs.r.value = currentColor[0].toString();
    sliderDOMs.g.value = currentColor[1].toString();
    sliderDOMs.b.value = currentColor[2].toString();
    valueDOMs.r.value = currentColor[0].toString();
    valueDOMs.g.value = currentColor[1].toString();
    valueDOMs.b.value = currentColor[2].toString();
    const hex = rgb_to_hex(...currentColor);
    /** @type {HTMLInputElement} */
    const hex_field = document.getElementById("value-hex");
    hex_field.value = hex;
    const rgb = `rgb(${currentColor[0]}, ${currentColor[1]}, ${currentColor[2]})`;
    const rgb_field = document.getElementById("value-rgb");
    rgb_field.value = rgb;
}

initColor();
