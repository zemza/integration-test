// src/routes.js
import { Router } from "express";
import { rgb_to_hex } from "./converter.js";
const routes = Router();
// Endpoint GET '{{api_url}}/'
routes.get('/', (req, res) => res.status(200).send("Welcome!"));

// Endpoint GET '{{api_url}}/rgb-to-hex?red=255&green=133&blue=0'
routes.get('/rgb-to-hex', (req, res) => {
// collect query parameters "red", "green" and "blue"
// sanitize and/or convert data
// convert RGB values to HEX value
// respond with HEX value
    const R = parseFloat(req.query.red);
    const G = parseFloat(req.query.green);
    const B = parseFloat(req.query.blue);
    const HEX = rgb_to_hex(R, G, B);
    res.status(200).send(HEX);
});
// Endpoint GET '{{api_url}}/hex-to-rgb'
routes.get('/hex-to-rgb', (req, res) => {
res.status(200).send("HEX-to-RGB not implemented yet.")
});
export default routes;